# mdpf

## v1.2 

### 2019-06-19-1

- Mise à jour importante de la documentation
    - accès à une version courte (fin marquée par `##`) et une version complète
    - mise à jour du fichier `modele-diaporama.md`
- Ajout options
    - `--man`, `--examples`, `--pandoc-extra`, `--workdir`
- Modification options
    - `-d` devient `-D`
    - `-t` sans paramètres liste les thèmes disponibles
- Réorganisation légère du code pour améliorer la lisibilité
- Ajout de la déprotection de certains commentaires avant le passage dans pandoc
